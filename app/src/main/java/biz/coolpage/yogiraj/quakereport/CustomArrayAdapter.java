package biz.coolpage.yogiraj.quakereport;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Gaurav on 27-01-2018.
 */

public class CustomArrayAdapter extends ArrayAdapter<HorizontalTexts> {

    public CustomArrayAdapter(@NonNull Context context, ArrayList<HorizontalTexts> horizontalTexts) {
        super(context, 0 ,horizontalTexts);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = convertView;

        if(view == null)
        {
            view = LayoutInflater.from(getContext()).inflate(R.layout.earthquake_activity,parent,false);
        }
        HorizontalTexts horizontalTexts = getItem(position);

        TextView magnitudeText = (TextView)view.findViewById(R.id.textMag);
        magnitudeText.setText(horizontalTexts.getMagnitude());
        GradientDrawable magnitudeColor = (GradientDrawable)magnitudeText.getBackground();
        int colors = getMagnitudeColor((horizontalTexts.getMagnitude()));
        int magColor = ContextCompat.getColor(getContext(),colors);
        magnitudeColor.setColor(magColor);

        TextView cityName = (TextView)view.findViewById(R.id.textCity);
        cityName.setText(horizontalTexts.getCityName().toString());

        TextView date = (TextView)view.findViewById(R.id.textDate);
        date.setText(horizontalTexts.getDate().toString());

        TextView areaName = (TextView)view.findViewById(R.id.textArea);
        areaName.setText(horizontalTexts.getAreaName());


        return view;
    }

    private  int getMagnitudeColor(String magg)
    {
        double mag = Double.parseDouble(magg);
        int colorId;
        int mags = (int)Math.floor(mag);

        switch(mags)
        {
            case 0:
            case 1:
                colorId = R.color.magnitude1;

                break;
            case 2:
                colorId = R.color.magnitude2;

                break;
            case 3:
                colorId = R.color.magnitude3;

                break;
            case 4:
                colorId = R.color.magnitude4;

                break;
            case 5:
                colorId = R.color.magnitude5;

                break;
            case 6:
                colorId = R.color.magnitude6;

                break;
            case 7:
                colorId = R.color.magnitude7;

                break;
            case 8:
                colorId = R.color.magnitude8;

                break;
            case 9:
                colorId = R.color.magnitude9;

                break;
            case 10:
                colorId = R.color.magnitude10;

                break;
            default:
                colorId = R.color.colorAccent;

                break;
        }




        return colorId;
    }
}
