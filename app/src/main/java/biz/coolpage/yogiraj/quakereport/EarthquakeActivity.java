package biz.coolpage.yogiraj.quakereport;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class EarthquakeActivity extends AppCompatActivity {

    private static Date d = new Date();
    private static  CharSequence s  = DateFormat.format("dd/MM/yyyy ", d.getTime());
    private static final String USGS_REQUEST_URL =
            "https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&orderby=time&minmag=6&limit=10";
    ListView listView;
    String primaryLocation;
    String locationOffset;
    AdView mAdView,mAdView1;
    private CustomArrayAdapter customArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);

        listView = (ListView) findViewById(R.id.listViewparent);
        MobileAds.initialize(this,"ca-app-pub-5119791385964626~8739824050");
        mAdView = (AdView)findViewById(R.id.mainadd);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView1 = (AdView)findViewById(R.id.mainadd1);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        mAdView1.loadAd(adRequest1);
        customArrayAdapter = new CustomArrayAdapter(this, new ArrayList<HorizontalTexts>());
        listView.setAdapter(customArrayAdapter);

        EarthQuakeAsyncTask task = new EarthQuakeAsyncTask();
        task.execute(USGS_REQUEST_URL);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HorizontalTexts horizontalTexts = customArrayAdapter.getItem(position);
                Intent browser = new Intent(Intent.ACTION_VIEW);
                browser.setData(Uri.parse(horizontalTexts.getUrl()));
                startActivity(browser);
            }
        });



    }


   private class EarthQuakeAsyncTask extends AsyncTask<String, Void, List<HorizontalTexts>>
   {

       @Override
       protected List<HorizontalTexts> doInBackground(String... strings) {

           if(strings.length < 1 || strings[0] == null)
           {
               return null;
           }
           List<HorizontalTexts> result = HttpRequestClass.fetchData(strings[0]);
           return result;
       }

       @Override
       protected void onPostExecute(List<HorizontalTexts> horizontalTexts) {
            customArrayAdapter.clear();

            if(horizontalTexts != null && !horizontalTexts.isEmpty())
            {
                customArrayAdapter.addAll(horizontalTexts);
            }
       }
   }


}
