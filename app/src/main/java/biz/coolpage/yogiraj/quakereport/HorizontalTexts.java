package biz.coolpage.yogiraj.quakereport;

/**
 * Created by Gaurav on 27-01-2018.
 */

public class HorizontalTexts {

    private String magnitude;
    private String cityName;
    private String date;
    private String areaName;
    private String url;



    public HorizontalTexts(String magnitude, String cityName, String date, String areaName, String url) {
        this.magnitude = magnitude;
        this.cityName = cityName;
        this.date = date;
        this.areaName = areaName;
        this.url = url;

    }

    public String getMagnitude() {

        return magnitude;
    }

    public String getCityName() {
        return cityName;
    }



    public String getDate() {
        return date;
    }

    public String getAreaName() {
        return areaName;
    }

    public String getUrl() {
        return url;
    }
}
