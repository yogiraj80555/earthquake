package biz.coolpage.yogiraj.quakereport;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Gaurav on 09-02-2018.
 */

public class HttpRequestClass {

    static String cityName;
    static String areaName;

    private static URL createUri(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);

        } catch (MalformedURLException e) {
        }
        return url;
    }


    private static String makeHttpRequest(URL url) {
        String jsonResponse = "";
        if (url == null) {
            return jsonResponse;
        }
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;

        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setConnectTimeout(35000);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            if (httpURLConnection.getResponseCode() == 200) {
                inputStream = httpURLConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            }

        } catch (IOException e) {
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }

        return jsonResponse;
    }


    private static String readFromStream(InputStream inputStream) {
        StringBuilder stringBuilder = new StringBuilder();
        if (inputStream == null) {

        } else {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            try {
                String line = bufferedReader.readLine();
                while (line != null) {
                    stringBuilder.append(line);
                    line = bufferedReader.readLine();
                }
            } catch (IOException e) {
            }
        }
        return stringBuilder.toString();
    }


    private static List<HorizontalTexts> extractFeatureFromJSON(String jsonString) {
        if (TextUtils.isEmpty(jsonString)) {
            return null;
        }
        List<HorizontalTexts> list = new ArrayList<>();

        try {
            JSONObject baseObject = new JSONObject(jsonString);
            JSONArray jsonArray = baseObject.getJSONArray("features");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject currentQuake = jsonArray.getJSONObject(i);
                JSONObject properties = currentQuake.getJSONObject("properties");
                double magnitudes = properties.getDouble("mag");
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                String magnitude = decimalFormat.format(magnitudes);

                String location = properties.getString("place");
                final String Location_Connect = " of";
                if (location.contains(Location_Connect)) {
                    String[] parts = location.split(Location_Connect);
                    areaName = parts[0] + Location_Connect;
                    cityName = parts[1];
                } else {
                    areaName = "Near the ";
                    cityName = location;
                }


                long time = properties.getLong("time");
                Date date = new Date(time);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, MMM d, yyyy,  h:mm a");
                String dateDisplay = simpleDateFormat.format(date);


                String url = properties.getString("url");
                HorizontalTexts texts = new HorizontalTexts(magnitude, cityName, dateDisplay, areaName, url);
                list.add(texts);
            }

        } catch (JSONException e) {
        }

        return list;
    }


    public static List<HorizontalTexts> fetchData(String stringUrl) {
        URL url = createUri(stringUrl);
        String jsonResponse = null;


        jsonResponse = makeHttpRequest(url);


        List<HorizontalTexts> lists = extractFeatureFromJSON(jsonResponse);

        return lists;
    }




}
